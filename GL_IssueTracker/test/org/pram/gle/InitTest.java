package org.pram.gle;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.gitlab.api.GitlabAPI;
import org.gitlab.api.models.GitlabIssue;
import org.gitlab.api.models.GitlabProject;
import org.gitlab.api.models.GitlabSession;
import org.junit.Before;
import org.junit.Test;

public class InitTest {

	private static final Logger logger = LogManager.getLogger(InitTest.class.getName());
	static {
		logger.debug(InitTest.class.getName() + " logger initialized");
	}
	
	private static final String USER = "user";
	private static final String PASS = "pass";
	
	private Properties props = new Properties();
	
	@Before
	public void setup() {
		try {
			props.load(new FileInputStream("test/test.properties"));
		} catch (Exception e) {
			logger.fatal("Could not load properties file", e);
			System.exit(-1);
		}
	}
	
	@Test
	public void testConnection() {
		
		try {
			GitlabSession session = GitlabAPI.connect("https://gitlab.com", props.getProperty(USER), props.getProperty(PASS));
			
			logger.debug(session.getName());
			GitlabAPI gapi = GitlabAPI.connect("https://gitlab.com", session.getPrivateToken());
			
			List<GitlabProject> projectList = gapi.getProjects();

			for( GitlabProject gp : projectList ) {
				logger.info("Project: " + gp.getName());
				List<GitlabIssue> issueList = gapi.getIssues(gp);
				for( GitlabIssue gi : issueList ) {
					logger.info("Title: " + gi.getTitle());
					logger.info("Description:\n" + gi.getDescription());
				}
			}
		} catch (IOException ex) {
			logger.error("Session fail", ex);
		}
	}
}
